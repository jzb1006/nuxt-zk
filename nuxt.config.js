const vuxLoader = require('vux-loader')
const bodyParser = require('body-parser')
const session = require('express-session')
const axios = require('axios')
module.exports = {

  head: {
    title: 'ssr-nuxt-express',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Nuxt.js project'
      }
    ],
    link: [{
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: '//at.alicdn.com/t/font_767395_7e0pl4798rm.css'
      }
    ],
    script: [{
        src: 'https://api.map.baidu.com/api?v=2.0&ak=F10c04ed0939110e024e319af6bad640&s=1'
      },
      {
        src: 'https://api.map.baidu.com/library/SearchInfoWindow/1.5/src/SearchInfoWindow_min.js'
      }
    ],
  },
  generate: {
    interval: 100,
    routes: function () {
      return [1, 2, 3, 4, 5].map((id) => {
        return {
          route: '/generate_container/' + id,
          payload: id
        }

      })
    }

  },
  serverMiddleware: [
    // body-parser middleware
    bodyParser.json(),
    // session middleware
    session({
      secret: 'super-secret-key',
      resave: false,
      saveUninitialized: false,
      cookie: {
        maxAge: 60000
      }
    }),
    // Api middleware
    // We add /api/login & /api/logout routes
    '~/api/index.js'
  ],

  modules: [
    '@nuxtjs/axios',
  ],


  //路由中间件--全局

  // router: {
  //   middleware :'auth'
  // },

  css: [
    // CSS file in the project
    '@/assets/css/main.css',
    // SCSS file in the project
    '@/assets/css/main.scss',
    '@/assets/css/Base.css',
    '@/assets/css/calandar.css',
    '@/assets/css/hospital.css',
    '@/assets/css/picview.css',
    '@/assets/css/theme.less',
    'vux/src/styles/reset.less',
    'vux/src/styles/1px.less',
    //阿里巴巴icon
    '@/assets/css/iconfont.css',
    'swiper/dist/css/swiper.css'
  ],

  plugins: [{
      src: '~/plugins/vux-plugins',
      ssr: false
    },
    {
      src: '~/plugins/vux-components',
      ssr: true
    },
    // {
    //   src: '~/plugins/http',
    //   ssr: true
    // },behavior.js
    {
      src: '~/plugins/behavior.js',
      ssr: false
    },
    {
      src: '~/plugins/component.js',
      ssr: true
    },
    {
      src: '~/plugins/self-components',
      ssr: true
    },
    {
      src: '~/plugins/nuxt-swiper-plugin.js',
      ssr: false
    },
    {
      src: '~/plugins/flexible.js',
      ssr: false
    }, //ssr false 是禁止静态化
    {
      src: '~/plugins/loading/index.js',
      ssr: false
    },
    {
      src: "~/plugins/vue-lazyload",
      ssr: false
    }
  ],


  vendor: ['axios', '~/plugins/loading/index.js'],

  loading: {
    color: '#3B8070'
  },

  build: {
    extend(config, {
      isDev,
      isClient
    }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

      const configs = vuxLoader.merge(config, {
        options: {
          ssr: true
        },
        plugins: ['vux-ui']
      
      })
      return configs
    },

    postcss: {
      
    },

  }
}
