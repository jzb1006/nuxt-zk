import Vue from "vue"
import VueLazyload from "vue-lazyload"
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: 'http://pic.qiantucdn.com/58pic/28/21/33/80c58PICx96_1024.jpg!/fw/1024/watermark/url/L2ltYWdlcy93YXRlcm1hcmsveGlhb3R1LnBuZw==/align/center',
    loading: 'http://pic.qiantucdn.com/58pic/13/15/64/54D58PIC5BF_1024.jpg!/fw/1024/watermark/url/L2ltYWdlcy93YXRlcm1hcmsveGlhb3R1LnBuZw==/align/center',
    lazyComponent: true
  })
