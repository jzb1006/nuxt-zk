import Vue from 'vue'
import {
  Tabbar,
  TabbarItem,
  // Spinner
  // CellFormPreview,
  // Group,
  // Cell,
  // Previewer,
  // TransferDom,
  // Divider 
} from 'vux'

Vue.component('Tabbar', Tabbar)
Vue.component('TabbarItem', TabbarItem)
// Vue.component('spinner', Spinner)
// Vue.component('CellFormPreview', CellFormPreview)
// Vue.component('Group', Group)
// Vue.component('Cell', Cell)
// Vue.component('Previewer', Previewer)
// Vue.component('TransferDom', TransferDom)
// Vue.component('Divider', Divider)
