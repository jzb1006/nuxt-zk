import vue from 'vue'
export const state = ()=>({
  latitude: '',
  longitude: '',
  city: "北京市",
  district: "朝阳区",
  province: "北京市",
  address: "北京市朝阳区",
  containerId:0,
  child_params:{}
})

const actions = {
  changeLatitude({
    commit
  }, latitude) {
    commit('CHANGE_LATITUDE', latitude)
  },
  changeLongitude({
    commit
  }, longitude) {
    commit('CHANGE_LONGITUTE', longitude)
  },
  changeCity({
    commit
  }, city) {
    commit('CHANGE_CITY', city)
  },
  changeDistrict({
    commit
  }, district) {
    commit('CHANGE_DISTRICT', district)
  },
  changeProvince({
    commit
  }, province) {
    commit('CHANGE_PROVINCE', province)
  },
  changeAddress({
    commit
  }, address) {
    commit('CHANGE_ADDRESS', address)
  },
  changeParams({
    commit
  }, child_params) {
    commit('CHANGE_PARAMS', child_params)
  },
  changeId({
    commit
  }, id) {
    commit('CHANGE_ID', id)
  },
}

const mutations = {
  CHANGE_LATITUDE(state, latitude) {
    state.latitude = latitude
  },
  CHANGE_LONGITUTE(state, longitude) {
    state.longitude = longitude
  },
  CHANGE_CITY(state, city) {
    state.city = city
  },
  CHANGE_DISTRICT(state, district) {
    state.district = district
  },
  CHANGE_PROVINCE(state, province) {
    state.province = province
  },
  CHANGE_ADDRESS(state, address) {
    state.address = address
  },
  CHANGE_PARAMS(state, child_params){
    state.child_params = child_params
  },
  CHANGE_ID(state, id){
    state.containerId = id
  },
}

export default {
    state,
    actions,
    mutations
}